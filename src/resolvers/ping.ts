// ruta de prueba - devolver un string
// Aprovechar type, para crear Querys - consultas
import { Query, Resolver} from 'type-graphql'

//export para utlizarlo en otro archivo

// Tipo de consulta que se hace desde GraphQL
@Resolver()
export class PingResolver {
    //  @Query : Decorador - devolver un string
    @Query(()=> String)
    ping(){
        return "Pong!"
    }
}