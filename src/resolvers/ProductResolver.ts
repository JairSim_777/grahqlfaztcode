// Arg = Argumento
import { Resolver, Query, Mutation, Arg } from 'type-graphql'; 

// model data entity. resolver Product: create, consult, save, delete data into MySQL
import { Product } from '../entity/Product'

// Creation of Mutation ProductResolver 
@Resolver()
export class ProductResolver {
    @Mutation(()=> Boolean)
    // function createProduct
    async createProduct(
        @Arg("name") name: string, 
        @Arg("quantity") quantity: number
    ){
        //  promise - Save product
        await Product.insert({name, quantity})
        console.log(name, quantity)
        return true
    }
}







