// file app.ts for configure the server
/*  Crear (definir) un servidor 
    Iniciar servidor http
    Definir una ruta de GraphQL
    Utilizar la definicion de type-graphql para crear Querys */

import express from 'express'; 
// Crear API de GraphQL
import { ApolloServer } from 'apollo-server-express'; 

// Recibir una clase creada con type-graphql y convetirla a un schema propio de GraphQL
import { buildSchema } from 'type-graphql'; 

import { PingResolver } from './resolvers/ping'; 
import { ProductResolver } from './resolvers/ProductResolver'; 


export async function startServer(){
    const app = express();

    
    // Crear (definir) ruta para el endpoint -GraphQL
    // para poder utilizar metodos asincronos
    const server = new ApolloServer({
        // schema: pasar a la conversion de buildSchema
        schema : await buildSchema({
            resolvers: [PingResolver, ProductResolver]
        }),
        context: ({req, res}) => ({req, res})
    })

    // Apollo va ha funcionar dentro de la ruta de express
    server.applyMiddleware({app, path: '/graphql'}); 

    return app; 
}

