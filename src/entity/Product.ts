import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, BaseEntity } from 'typeorm'; 

@Entity()
export class Product extends BaseEntity {
    // @PrimaryGeneratedColumn() the id of table is created automatically - by default
    @PrimaryGeneratedColumn()
    id!: number; 
    
    @Column()
    name!: string;
    
    // when the column does not have a value greater than zero, set the default zero
    @Column("int", {default: 0})
    quantity!: number; 

    //Date
    @CreateDateColumn({type: 'timestamp'})
    createdAt!: string; 

}