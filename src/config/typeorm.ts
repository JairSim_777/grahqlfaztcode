import { createConnection } from 'typeorm'; 
import path from 'path'; 

// function connect to db My SQL
export async function connect(){
    await createConnection ({
        type: 'mysql',
        // where the database is stored
        host: 'localhost',
        port: 3306, 
        username: 'root', 
        password: 'root', 
        // name database graphqlts
        database: 'graphqlts', 
        entities: [
            path.join(__dirname, '../entity/**/**.ts')
        ], 
        synchronize: true
    });
    console.log('Database is connected');
}