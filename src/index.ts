// file index.ts is for inicialize the server
//console.log('hello ')

import "reflect-metadata";
import { connect } from './config/typeorm'; 
import { startServer } from './app';



async function main(){
    // connection DB
    connect()
    // initialize server asyn 
    const app = await startServer();
    app.listen(3000); 
    console.log('Server on port', 3000); 
}

main(); 